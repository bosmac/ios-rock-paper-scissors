//
//  ResultViewController.m
//  Rock-Paper-Scissor
//
//  Created by Marcelo Macri on 7/28/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

NSInteger winner;

- (void) setupMySelection{
    [_myNameLabel setText:_myName];
    switch (_mySelectionNumber) {
        case 0:
            [_mySelectionImage setImage:_rockImage];
            break;
        case 1:
            [_mySelectionImage setImage:_paperImage];
            break;
        case 2:
            [_mySelectionImage setImage:_scissorImage];
            break;
        default:
            break;
    }
}

- (void) setupOpponentSelection{
    [_opponentNameLabel setText:_opponentName];
    switch (_opponentSelectionNumber) {
        case 0:
            [_opponentSelectionImage setImage:_rockImage];
            break;
        case 1:
            [_opponentSelectionImage setImage:_paperImage];
            break;
        case 2:
            [_opponentSelectionImage setImage:_scissorImage];
            break;
        default:
            break;
    }
}

- (void) determineWinner{
    // TIE = 0
    // WIN = 1
    // LOSE = 2;
    
    if(_mySelectionNumber == 0 && _opponentSelectionNumber == 0){
        NSLog(@"rock vs rock: ITS A TIE");
        winner = 0;
    } else if(_mySelectionNumber == 0 && _opponentSelectionNumber == 1){
        NSLog(@"rock vs paper: I LOSE");
        winner = 2;
    } else if(_mySelectionNumber == 0 && _opponentSelectionNumber == 2){
        NSLog(@"rock vs scissor: I WIN");
        winner = 1;
    }
    
    else if(_mySelectionNumber == 1 && _opponentSelectionNumber == 0){
        NSLog(@"paper vs rock: I WIN");
        winner = 1;
    } else if(_mySelectionNumber == 1 && _opponentSelectionNumber == 1){
        NSLog(@"paper vs paper: ITS A TIE");
        winner = 0;
    } else if(_mySelectionNumber == 1 && _opponentSelectionNumber == 2){
        NSLog(@"paper vs scissor: I LOSE");
        winner = 2;
    }
    
    else if(_mySelectionNumber == 2 && _opponentSelectionNumber == 0){
        NSLog(@"scissor vs rock: I LOSE");
        winner = 2;
    } else if(_mySelectionNumber == 2 && _opponentSelectionNumber == 1){
        NSLog(@"scissor vs paper: I WIN");
        winner = 1;
    } else if(_mySelectionNumber == 2 && _opponentSelectionNumber == 2){
        NSLog(@"scissor vs scissor: ITS A TIE");
        winner = 0;
    }
}

- (void) displayWinnerAlert {
    // TIE = 0
    // WIN = 1
    // LOSE = 2
    
    UIAlertView *alert;
    
    switch (winner) {
        case 0:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"ITS A TIE !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        case 1:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"YOU WIN !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        case 2:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"YOU LOSE !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _rockImage = [UIImage imageNamed:@"rock.png"];
    _paperImage = [UIImage imageNamed:@"paper.png"];
    _scissorImage = [UIImage imageNamed:@"scissor.png"];
    
    NSLog(@"my name: %@", _myName);
    NSLog(@"opponent name: %@", _opponentName);
    NSLog(@"my selection number: %d", _mySelectionNumber);
    NSLog(@"opponent selection number: %d", _opponentSelectionNumber);

    [self setupMySelection];
    [self setupOpponentSelection];
    [self determineWinner];
    [self displayWinnerAlert];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
