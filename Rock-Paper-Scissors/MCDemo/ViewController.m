//
//  ViewController.m
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "ResultViewController.h"

@interface ViewController ()

@property (nonatomic, strong) AppDelegate *appDelegate;

-(void)sendMyMessage;
-(void)didReceiveDataWithNotification:(NSNotification *)notification;

@end

@implementation ViewController

NSInteger mySelectionNumber;
NSInteger opponentSelectionNumber;
NSString *myName;
NSString *opponentName;

NSInteger sentSuccess;
NSInteger receivedSuccess;

- (IBAction) goButtonPressed {
    NSLog(@"go Button Pressed");
    NSLog(@"Number of session connected peers: %d", _appDelegate.mcManager.session.connectedPeers.count);
    NSLog(@"size of connected peer array %d", _appDelegate.mcManager.connectedPeers.count);
    NSLog(@"name of connected peer %@", _appDelegate.mcManager.connectedPeers);

    if (_appDelegate.mcManager.session.connectedPeers.count > 0) {
        [_goButton setTitle:@"LOADING..." forState:UIControlStateNormal];
        [_goButton setEnabled:false];
        [_rpsSegmentControl setEnabled:false];
        [self sendMyMessage];
        [self pushResultViewController];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@""
                              message:@"Make sure you are connected to another player before clicking go.\n"
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void) pushResultViewController{
    NSLog(@"Push result view controller");
    if (sentSuccess == 1 && receivedSuccess == 1) {
        
        ResultViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultVC"];
        rvc.myName = myName;
        rvc.mySelectionNumber = mySelectionNumber;
        rvc.opponentName = opponentName;
        rvc.opponentSelectionNumber = opponentSelectionNumber;
        [_goButton setEnabled:true];
        [_rpsSegmentControl setEnabled:true];
        [_goButton setTitle:@"GO !!!" forState:UIControlStateNormal];
        sentSuccess = 0;
        receivedSuccess = 0;
        [self.navigationController pushViewController:rvc animated:YES];
        
    } else {
        NSLog(@"sentSuccess: %d receivedSuccess: %d", sentSuccess, receivedSuccess);
        [NSTimer scheduledTimerWithTimeInterval:2.0
                                         target:self
                                       selector:@selector(pushResultViewController)
                                       userInfo:nil
                                        repeats:NO];
    }
}

- (IBAction) rpsSegmentControlPressed {
    NSLog(@"rpsSegmentControlPressed");
    mySelectionNumber = _rpsSegmentControl.selectedSegmentIndex;
    NSLog(@"selected segment index: %d", mySelectionNumber);
    switch (mySelectionNumber) {
        case 0:
            [_rpsImageView setImage:_rockImage];
            break;
        case 1:
            [_rpsImageView setImage:_paperImage];
            break;
        case 2:
            [_rpsImageView setImage:_scissorImage];
            break;
        default:
            break;
    }
}

-(void)sendMyMessage{
    NSLog(@"send my message");
    NSString *temp = [NSString stringWithFormat:@"%d", mySelectionNumber];
    NSData *dataToSend = [temp dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *allPeers = _appDelegate.mcManager.session.connectedPeers;
    NSError *error;
    
    [_appDelegate.mcManager.session sendData:dataToSend
                                     toPeers:allPeers
                                    withMode:MCSessionSendDataReliable
                                       error:&error];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"There was a connection error.  Please try again."
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil];
        [alert show];
        [_goButton setEnabled:true];
        [_rpsSegmentControl setEnabled:true];
    } else {
        sentSuccess = 1;
    }
}

-(void)didReceiveDataWithNotification:(NSNotification *)notification{
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;
    NSData *receivedData = [[notification userInfo] objectForKey:@"data"];
    NSString *receivedText = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSInteger temp = [receivedText integerValue];
    opponentName = peerDisplayName;
    opponentSelectionNumber = temp;
    NSLog(@"%@ %d", opponentName, opponentSelectionNumber);
    receivedSuccess = 1;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _rockImage = [UIImage imageNamed:@"rock.png"];
    _paperImage = [UIImage imageNamed:@"paper.png"];
    _scissorImage = [UIImage imageNamed:@"scissor.png"];
    
    NSString *name = [[UIDevice currentDevice] name];
    myName = name;
    mySelectionNumber = _rpsSegmentControl.selectedSegmentIndex;
    
    sentSuccess = 0;
    receivedSuccess = 0;
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (_appDelegate.mcManager.session.connectedPeers.count < 1) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@""
                              message:@"Connect to another player, then select either rock, paper, or scissor, then click go!!!\n"
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil];
        [alert show];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveDataWithNotification:)
                                                 name:@"MCDidReceiveDataNotification"
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
