//
//  ConnectionsViewController.m
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import "ConnectionsViewController.h"
#import "AppDelegate.h"


@interface ConnectionsViewController ()

@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSMutableArray *arrConnectedDevices;

-(void)peerDidChangeStateWithNotification:(NSNotification *)notification;

@end

@implementation ConnectionsViewController

/*
 -(BOOL)textFieldShouldReturn:(UITextField *)textField{
 [_txtName resignFirstResponder];
 
 _appDelegate.mcManager.peerID = nil;
 _appDelegate.mcManager.session = nil;
 _appDelegate.mcManager.browser = nil;
 
 if ([_swVisible isOn]) {
 [_appDelegate.mcManager.advertiser stop];
 }
 _appDelegate.mcManager.advertiser = nil;
 
 
 [_appDelegate.mcManager setupPeerAndSessionWithDisplayName:_txtName.text];
 [_appDelegate.mcManager setupMCBrowser];
 [_appDelegate.mcManager advertiseSelf:_swVisible.isOn];
 
 return YES;
 }
 */

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"alertview clicked button disconnect");
    [_appDelegate.mcManager.session disconnect];
    [_arrConnectedDevices removeAllObjects];
    [_tblConnectedDevices reloadData];
}

-(void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController{
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
    
    if (_appDelegate.mcManager.session.connectedPeers.count > 1) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@""
                              message:@"You can only connect to one peer. Please disconnect all peers and try again.\n"
                              delegate:self
                              cancelButtonTitle:@"Disconnect"
                              otherButtonTitles: nil];
        [alert show];
    }else{
        NSString *temp = [NSString stringWithFormat:@"%d", _appDelegate.mcManager.session.connectedPeers.count];
        NSLog(@"connected peers: %@", temp);
        [_tblConnectedDevices reloadData];
    }
}

-(void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController{
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
    
    if (_appDelegate.mcManager.session.connectedPeers.count > 1) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@""
                              message:@"You can only connect to one peer. Please disconnect all peers and try again.\n"
                              delegate:self
                              cancelButtonTitle:@"Disconnect"
                              otherButtonTitles: nil];
        [alert show];
    }else{
        NSString *temp = [NSString stringWithFormat:@"%d", _appDelegate.mcManager.session.connectedPeers.count];
        NSLog(@"connected peers: %@", temp);
        [_tblConnectedDevices reloadData];
    }
    
}

- (IBAction)browseForDevices:(id)sender{
    NSLog(@"browse for devices");
    [[_appDelegate mcManager] setupMCBrowser];
    [[[_appDelegate mcManager] browser] setDelegate:self];
    [self presentViewController:[[_appDelegate mcManager] browser] animated:YES completion:nil];
}

- (IBAction)toggleVisibility:(id)sender{
    NSLog(@"toggle visibility");
    [_appDelegate.mcManager advertiseSelf:_swVisible.isOn];
}

- (IBAction)disconnect:(id)sender{
    NSLog(@"disconnect");
    [_appDelegate.mcManager.session disconnect];
    
    //_txtName.enabled = YES;
    
    [_arrConnectedDevices removeAllObjects];
    [_tblConnectedDevices reloadData];
}

-(void)peerDidChangeStateWithNotification:(NSNotification *)notification{
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;
    
    MCSessionState state = [[[notification userInfo] objectForKey:@"state"] intValue];
    
    if (state != MCSessionStateConnecting) {
        if (state == MCSessionStateConnected) {
            [_arrConnectedDevices addObject:peerDisplayName];
            [[_appDelegate mcManager]setConnectedPeers:_arrConnectedDevices];
        }
        else if (state == MCSessionStateNotConnected){
            if ([_arrConnectedDevices count] > 0) {
                int indexOfPeer = [_arrConnectedDevices indexOfObject:peerDisplayName];
                [_arrConnectedDevices removeObjectAtIndex:indexOfPeer];
                [[_appDelegate mcManager]setConnectedPeers:_arrConnectedDevices];
            }
        }
        [_tblConnectedDevices reloadData];
        
        //BOOL peersExist = ([[_appDelegate.mcManager.session connectedPeers] count] == 0);
        //[_btnDisconnect setEnabled:!peersExist];
        //[_txtName setEnabled:peersExist];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrConnectedDevices count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier"];
    }
    
    cell.textLabel.text = [_arrConnectedDevices objectAtIndex:indexPath.row];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *name = [[UIDevice currentDevice] name];
    [_txtName setText:name];
    [_txtName setEnabled:false];
    [_txtName setDelegate:self];
    _arrConnectedDevices = [[NSMutableArray alloc] init];
    
    if (!_appDelegate.mcManager.session) {
        NSLog(@"view did load - session does not exist. create new session");
        [[_appDelegate mcManager] setupPeerAndSessionWithDisplayName:name];
        [[_appDelegate mcManager] advertiseSelf:_swVisible.isOn];
    }else{
        NSString *temp = [NSString stringWithFormat:@"%d", _appDelegate.mcManager.session.connectedPeers.count];
        NSString *temp2 = [NSString stringWithFormat:@"%@", _appDelegate.mcManager.session.connectedPeers];
        NSLog(@"view did load session already exist. connected peers count: %@, name: %@", temp, temp2);
        if (_appDelegate.mcManager.session.connectedPeers.count > 0) {
            [self setArrConnectedDevices:_appDelegate.mcManager.connectedPeers];
        }
    }
    
    [_tblConnectedDevices setDelegate:self];
    [_tblConnectedDevices setDataSource:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(peerDidChangeStateWithNotification:)
                                                 name:@"MCDidChangeStateNotification"
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
