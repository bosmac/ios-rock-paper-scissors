//
//  ViewController.h
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewController.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *goButton;
@property (nonatomic, strong) IBOutlet UISegmentedControl *rpsSegmentControl;
@property (nonatomic, strong) IBOutlet UIImageView *rpsImageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) UIImage *rockImage;
@property (nonatomic, strong) UIImage *paperImage;
@property (nonatomic, strong) UIImage *scissorImage;

- (IBAction) goButtonPressed;
- (IBAction) rpsSegmentControlPressed;


@end
