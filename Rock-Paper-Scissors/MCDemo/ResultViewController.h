//
//  ResultViewController.h
//  Rock-Paper-Scissor
//
//  Created by Marcelo Macri on 7/28/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property NSInteger mySelectionNumber;
@property NSInteger opponentSelectionNumber;
@property NSString *myName;
@property NSString *opponentName;

@property (nonatomic, strong) UIImage *rockImage;
@property (nonatomic, strong) UIImage *paperImage;
@property (nonatomic, strong) UIImage *scissorImage;

@property (nonatomic, strong) IBOutlet UIImageView *mySelectionImage;
@property (nonatomic, strong) IBOutlet UIImageView *opponentSelectionImage;
@property (nonatomic, strong) IBOutlet UILabel *myNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *opponentNameLabel;


@end
